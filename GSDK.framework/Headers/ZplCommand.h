//
//  ZplCommand.h
//  GSDK
//
//  Created by max on 2021/3/1.
//  Copyright © 2021 max. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZplConfig.h"
#import <UIKit/UIKit.h>
@interface ZplCommand : NSObject

/**
 * 方法说明：标签起始，起始命令必须以这个开始
 */
- (void) startZpl;

/**
 * 方法说明：标签结束，最后命令必须以这个结束
 */
- (void) endZpl;

/**
 * 方法说明：获得打印命令
*/
-(NSData*) getCommand;

/**
 * 方法说明：执行打印
 * @param m 指定打印的份数（set）1≤m≤99999999
 * @param n 每张拷贝数 默认值：0(不复制) 每张标签需重复打印的张数 1≤n≤99999999
 * @param pauseCut 多少张后暂停，默认0(不暂停)
 * @param isPause 默认值YES，如果参数设定为 YES，打印机打印不会有暂停操作，如果设定为 NO，打印机每打印一组标签就会暂停，直到用户按下 FEED
 */
-(void)addPrintNum:(int) m copy:(int) n pauseCut:(int) pauseCut isPause:(BOOL) isPause;


/**
 * 方法说明：设置标签尺寸的宽和高
 * @param width  标签宽度
 * @param height 标签高度
 */
-(void) addSize:(int) width :(int) height;

/**
 * 方法说明：设置标签原点坐标
 * @param x  横坐标
 * @param y  纵坐标
 */
-(void) addReference:(int) x :(int)y;

/**
 * 方法说明：设置打印速度：默认3，其他值：2 ～ 5
 * 2 = 50.8 mm/sec. (2 inches/sec.)
 * 3 = 76.2 mm/sec. (3 inches/sec.)
 * 4 = 101.6 mm/sec. (4 inches/sec.)
 * 5 = 127 mm/sec.(5 inches/sec.)
 * @param speed  打印速度
*/
-(void) addSpeed:(int) speed;

/**
 * 方法说明：设置打印浓度
 * @param density  浓度：0 ～ 30
 */
-(void) addDensity:(int) density;

/**
 * 方法说明：打印镜像标签
 * @param isMirror  M 指令将整体的标签内容镜像打印出来。指令将图像左右颠倒过来
 */
-(void) addMirror:(BOOL) isMirror;

/**
 * 方法说明：倒置打印标签
 * @param isInvert  方向指令将整体的标签内容转过 180 度
 */
-(void) addInvertOrientation:(BOOL) isInvert;

/**
 * 方法说明:在标签上绘制文字
 * @param x 横坐标
 * @param y 纵坐标
 * @param font  字体名称 默认值：0 其他值：A-Z，0-9（打印机的任何字体，包括下载字体，EPROM 中储存的，当然这些字体必须用^CW 来定义为 A-Z，0-9）
 * @param rotation  旋转角度 N = 正常 （Normal) R = 顺时针旋转 90 度（Roated) I = 顺时针旋转 180 度（Inverted) B = 顺时针旋转 270 度 (Bottom)
 * @param fontWid  字符宽度
 * @param fontHei 字符高度
 * @param text   文字字符串
 */
-(void) addTextwithX:(int)x withY:(int)y withFont:(NSString*)font withRotation:(NSString *)rotation withFontWid:(int)fontWid withFontHei:(int)fontHei withText:(NSString*) text;

/**
 * 方法说明：打印图片
 * @param x 横坐标
 * @param y 纵坐标
 * @param width  图片宽度
 * @param image  图片源
*/
-(void)addBitmapwithX:(int)x withY:(int)y withWidth:(int)width withImage:(UIImage *)image;

/**
 * 方法说明：打印QR码
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addQRCode:(NSString*) content x:(int)x y:(int)y config:(QrCodeConfig *)config;

/**
 * 方法说明：打印CODE128码
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addCODE128:(NSString*) content x:(int)x y:(int)y config:(BarCodeConfig *)config;

/**
 * 方法说明：打印EAN8码:
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addEAN8:(NSString*)content x:(int)x y:(int)y config:(BarCodeConfig *)config;

/**
 * 方法说明：打印EAN13码
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addEAN13:(NSString*)content x:(int)x y:(int)y config:(BarCodeConfig *)config;

/**
 * 方法说明：打印UPCA条码
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addUPCA:(NSString*) content x:(int)x y:(int)y config:(BarCodeConfig *)config;
    
/**
 * 方法说明：打印UPCE条码
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addUPCE:(NSString*) content x:(int)x y:(int)y config:(BarCodeConfig *)config;

/**
 * 方法说明：打印CODE39条码
 * @param content 条码内容
 * @param x 横坐标
 * @param y 纵坐标
 * @param config  条码配置
*/
-(void) addCODE39:(NSString*) content x:(int)x y:(int)y config:(BarCodeConfig *)config;

/**
 * 方法说明：画边框
 * @param x 横坐标
 * @param y 纵坐标
 * @param w  框宽度，若设置宽为0，可实现竖线
 * @param h  框高度，若设置高为0，可实现横线
 * @param b  线宽
 * @param r  边框圆角值，默认值：0 许可值：0～8
*/
- (void)addGraphicBoxX:(int)x y:(int)y w:(int)w h:(int)h border:(int)b rounding:(int)r;

/**
 * 方法说明：画圆
 * @param x 横坐标
 * @param y 纵坐标
 * @param d  圆直径
 * @param b  线宽
*/
- (void)addGraphicCircleX:(int)x y:(int)y diameter:(int)d border:(int)b;

/**
 * 方法说明：画斜线
 * @param x 横坐标
 * @param y 纵坐标
 * @param w  长
 * @param h  高
 * @param b  线宽
 * @param o  斜线方向：默认右倾线：输入“R”，其他值：左倾线，输入“L”
*/
- (void)addGraphicDiagonalLineX:(int)x y:(int)y w:(int)w h:(int)h border:(int)b Orientation:(NSString *)o;

/**
 * 方法说明：画椭圆
 * @param x 横坐标
 * @param y 纵坐标
 * @param w  椭圆宽度
 * @param h  椭圆高度
 * @param b  线宽
*/
- (void)addGraphicEllipseX:(int)x y:(int)y w:(int)w h:(int)h border:(int)b;


@end

